#include<stdlib.h>
#include<time.h>
#include "cyber.h"
#include "utils.h"

using namespace std;
using namespace UTILS;

CYBER::CYBER(int num, string inputFile, double betaVal, double alphaval)
{
	NumActions = num;
	NumObservations = 2;
	srand(time(NULL));
	beta = betaVal;
	alpha = alphaval;
	numNodes = num;
	ifstream file(inputFile.c_str());
	
	double a;
	int index=0;
	while(file>>a)
	{
		if (index<numNodes)
			firewallStrengths.push_back(a);
		else
			nodeCosts.push_back(a);
		index++;
	}
	file.close();

/*	cout<<"Firewall strengths: \n";
	for (int i=0;i<numNodes;i++)
		cout<<firewallStrengths[i]<<" ";
	cout<<"\n";
	
	 cout<<"Node costs: \n";
    for (int i=0;i<numNodes;i++)
        cout<<nodeCosts[i]<<" ";
    cout<<"\n";*/
}

bool CYBER::Step(STATE& state, int action, 
        int& observation, double& reward) const
{}


STATE* CYBER::Copy(const STATE& state) const
{
    const CYBER_STATE& pocstate = safe_cast<const CYBER_STATE&>(state);
    CYBER_STATE* newstate = MemoryPool.Allocate();
    *newstate = pocstate;
    return newstate;
}

void CYBER::Validate(const STATE& state) const
{
    const CYBER_STATE& pocstate = safe_cast<const CYBER_STATE&>(state);
    for (int g = 0; g < numNodes; g++)
        assert(pocstate.Infected[g]==1 || pocstate.Infected[g]==0);
}

STATE* CYBER::CreateStartState() const
{
    CYBER_STATE* startState = MemoryPool.Allocate();

	int random = rand()%numNodes;
	for (int i=0;i<numNodes;i++)
	{
		if (i==random)
			startState->Infected.push_back(1);
		else
			startState->Infected.push_back(0);
	}

    return startState;
}

void CYBER::FreeState(STATE* state) const
{
    CYBER_STATE* pocstate = safe_cast<CYBER_STATE*>(state);
    MemoryPool.Free(pocstate);
}


bool CYBER::Step(STATE& state, int action,
    int& observation, double& reward, double& costsPaid, double &nodesInfected) const
{
    CYBER_STATE& pocstate = safe_cast<CYBER_STATE&>(state);

	int numInfected=0;	
	for (int i=0;i<pocstate.Infected.size();i++)
        if (pocstate.Infected[i]==1)
			numInfected++;

//	cout<<"NumInfected Before: "<<numInfected<<"\n";


	for (int i=0;i<pocstate.Infected.size();i++)
	{
		if (pocstate.Infected[i]==1)
		{
			if (action!=i)///you don't inspect node i
				continue;
			else
			{
				double prob = 1 - pow(firewallStrengths[i],numInfected+1);
				cout<<"real prob: "<<prob<<"\n";
				double r = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
				if (r<prob)
					pocstate.Infected[i]=1;
				else
					pocstate.Infected[i]=0;
			}		
		}
		else if (pocstate.Infected[i]==0)
		{
			if (action!=i)
			{
				double prob = 1 - pow(firewallStrengths[i],numInfected);
				cout<<"real prob: "<<prob<<"\n";
                double r = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
                if (r<prob)
                    pocstate.Infected[i]=1;
                else
                    pocstate.Infected[i]=0;
			}
			else
			{
				double prob = 1 - pow(firewallStrengths[i],numInfected+1);
				cout<<"real prob: "<<prob<<"\n";
                double r = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
                if (r<prob)
                    pocstate.Infected[i]=1;
                else
                    pocstate.Infected[i]=0;
			}
		}
	}

	observation = pocstate.Infected[action];
	
	int finalInfected=0;
	for (int i=0;i<pocstate.Infected.size();i++)
    	if (pocstate.Infected[i]==1)
        	finalInfected++;
	
	//cout<<"NumInfected after: "<<finalInfected<<"\n";
	
	//reward = finalInfected - numInfected;
	reward = -1*alpha*nodeCosts[action] - beta*(finalInfected-numInfected);
	costsPaid = nodeCosts[action];
	nodesInfected = finalInfected - numInfected;

	return false;
}


bool CYBER::LocalMove(STATE& state, const HISTORY& history,
    int stepObs, const STATUS& status) const
{
    CYBER_STATE& pocstate = safe_cast<CYBER_STATE&>(state);
	
	////randomly chosen action
	int action = rand()%numNodes;

	int numInfected=0;
    for (int i=0;i<pocstate.Infected.size();i++)
        if (pocstate.Infected[i]==1)
            numInfected++;


    for (int i=0;i<pocstate.Infected.size();i++)
    {
        if (pocstate.Infected[i]==1)
        {
            if (action!=i)///you don't inspect node i
                continue;
            else
            {
                double prob = 1 - pow(firewallStrengths[i],numInfected+1);
                double r = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
                if (r<prob)
                    pocstate.Infected[i]=1;
                else
                    pocstate.Infected[i]=0;
            }
        }
        else if (pocstate.Infected[i]==0)
        {
            if (action!=i)
            {
                double prob = 1 - pow(firewallStrengths[i],numInfected);
                double r = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
                if (r<prob)
                    pocstate.Infected[i]=1;
                else
                    pocstate.Infected[i]=0;
            }
            else
            {
                double prob = 1 - pow(firewallStrengths[i],numInfected+1);
                double r = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
                if (r<prob)
                    pocstate.Infected[i]=1;
                else
                    pocstate.Infected[i]=0;
            }
        }
    }

	return true;
}

void CYBER::GenerateLegal(const STATE& state, const HISTORY& history, 
    vector<int>& legal, const STATUS& status) const
{
    const CYBER_STATE& pocstate = safe_cast<const CYBER_STATE&>(state);

	for (int i=0;i<pocstate.Infected.size();i++)
		legal.push_back(i);
}

void CYBER::GeneratePreferred(const STATE& state, const HISTORY& history, 
    vector<int>& actions, const STATUS& status) const
{
    const CYBER_STATE& pocstate = safe_cast<const CYBER_STATE&>(state);
	
	for (int i=0;i<pocstate.Infected.size();i++)
        actions.push_back(i);
}

void CYBER::DisplayBeliefs(const BELIEF_STATE& beliefState,
    ostream& ostr) const
{
}

void CYBER::DisplayState(const STATE& state, ostream& ostr) const
{
	cout<<"State: \n";
	const CYBER_STATE& pocstate = safe_cast<const CYBER_STATE&>(state);
	
	for (int i=0;i<pocstate.Infected.size();i++)
		ostr<<pocstate.Infected[i]<<" ";
	ostr<<"\n";
}

void CYBER::DisplayObservation(const STATE& state, int observation, ostream& ostr) const
{
	ostr<<"Observation: "<<observation<<"\n";
}

void CYBER::DisplayAction(int action, ostream& ostr) const
{
    ostr << "Action: "<<action<<"\n";
}
