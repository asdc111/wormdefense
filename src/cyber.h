#ifndef CYBER_H
#define CYBER_H

#include<fstream>
#include<vector>
#include<string>
#include "simulator.h"
#include "beliefstate.h"

using namespace std;

class CYBER_STATE : public STATE
{
public:

    vector<int> Infected;
};

class CYBER : public SIMULATOR
{
public:

    virtual STATE* Copy(const STATE& state) const;
    virtual void Validate(const STATE& state) const;
    virtual STATE* CreateStartState() const;
    virtual void FreeState(STATE* state) const;
    virtual bool Step(STATE& state, int action, 
        int& observation, double& reward, double&a, double& b) const;
    virtual bool Step(STATE& state, int action, 
        int& observation, double& reward) const;

    virtual bool LocalMove(STATE& state, const HISTORY& history,
        int stepObs, const STATUS& status) const;
    void GenerateLegal(const STATE& state, const HISTORY& history,
        std::vector<int>& legal, const STATUS& status) const;
    void GeneratePreferred(const STATE& state, const HISTORY& history,
        std::vector<int>& legal, const STATUS& status) const;

    virtual void DisplayBeliefs(const BELIEF_STATE& beliefState, 
        std::ostream& ostr) const;
    virtual void DisplayState(const STATE& state, std::ostream& ostr) const;
    virtual void DisplayObservation(const STATE& state, int observation, std::ostream& ostr) const;
    virtual void DisplayAction(int action, std::ostream& ostr) const;

    double alpha;
    double beta;
    int numNodes;
    vector<double> firewallStrengths;
    vector<double> nodeCosts;


    CYBER(int numNodes, string inputFile, double beta, double alpha);


private:

    mutable MEMORY_POOL<CYBER_STATE> MemoryPool;
};

#endif // CYBER_H
