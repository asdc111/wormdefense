#include<iostream>
#include<stdlib.h>
#include<time.h>
#include<fstream>

using namespace std;

int main(int argc, char *argv[])
{
	srand(time(NULL));
	int costDistribution[] = {3, 14, 84, 93, 94, 100}; 
	int numNodes=atoi(argv[1]);

	ofstream file(argv[2]);

	/////firewall strengths...uniform random for now
	for (int i=0;i<numNodes;i++)
	{
		float r = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX))*0.1 + 0.9;
		file<<r<<" ";
	}
	file<<"\n";
	
	/////node costs
	for (int i=0;i<numNodes;i++)
	{
		int randval = rand()%100;
		int randcost =1;
		for (int j=0;j<6;j++, randcost++)
		{
			if (randval<=costDistribution[j])
				break;
		}
		
		file<<randcost<<" ";
	}
	file<<"\n";
	file.close();
	
}
