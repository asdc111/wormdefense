#!/bin/bash


for i in $(seq 50 50 500)
do
calum=0
cost=0
infect=0
te=50
for j in $(seq 1 1 $te)
do

./a.out "$i" input.txt

asha=$(./pomcp --problem cyber --numNodes "$i" --inputFile input.txt --alpha 1 --beta 1 --random false --nodebased false --firewallbased false --outputfile temp.txt --runs 1 --mindoubles 5 --maxdoubles 5 --accuracy 0.1 --horizon 2 --verbose 0 --autoexploration false --timeout 10000 --userave false --ravediscount 1.0 --raveconstant 0.01 --disabletree false --exploration 0.4 --usetransforms true | grep -e "+-" -e "Costs" -e "Infections")

#asha=$(./pomcp --problem cyber --numNodes "$i" --inputFile input.txt --alpha 0 --beta 1 --random false --nodebased false --firewallbased false --outputfile temp.txt --runs 1 --mindoubles 10 --maxdoubles 10 --accuracy 0.1 --horizon 10 --verbose 0 --autoexploration false --timeout 10000 --userave false --ravediscount 1.0 --raveconstant 0.01 --disabletree false --exploration 0.4 --usetransforms true | grep -e "+-" -e "Costs" -e "Infections")  


#echo $asha

#echo $asha | grep "Undiscounted" |  tail -n 1 | cut -f10 -d" "

#echo $asha | grep "Costs" | cut -f3 -d" "

#echo $asha | grep "Infections" | cut -f6 -d" "

ab=$(echo $asha | grep "Undiscounted" |  tail -n 1 | cut -f10 -d" ")

lo=$(echo $asha | grep "Costs" | cut -f3 -d" ")

ffo=$(echo $asha | grep "Infections" | cut -f6 -d" ")

#echo $ab
#echo $lo
#echo $ffo

calum=`echo "$calum" + "$ab" | bc`

cost=`echo "$cost" + "$lo" | bc`

infect=`echo "$infect" + "$ffo" | bc`
done

calum=`echo "$calum" / "$te" | bc`
cost=`echo "$cost" / "$te" | bc`
infect=`echo "$infect" / "$te" | bc`

echo $calum $cost $infect > out"$i".txt
done
